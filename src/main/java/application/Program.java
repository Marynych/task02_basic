package application;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter first number: ");
        int firstNumber = scanner.nextInt();
        System.out.print("Enter second number: ");
        int secondNumber = scanner.nextInt();
        int sum = 0;
        if (firstNumber < secondNumber) {
            for (int i = firstNumber; i <= secondNumber; i++) {
                if (i % 2 != 0) {
                    System.out.print(i);
                }
            }
            for (int i = secondNumber; i >= firstNumber; i--) {
                if (i % 2 != 0) {
                    System.out.print(i);
                }
            }
        } else if (firstNumber > secondNumber) {
            for (int i = secondNumber; i <= firstNumber; i++) {
                if (i % 2 != 0) {
                    System.out.print(i);
                }
            }
            for (int i = firstNumber; i >= secondNumber; i--) {
                if (i % 2 != 0) {
                    System.out.print(i);
                }
            }
        } else {
            System.out.print("numbers are equal");
        }
        int first = firstNumber;
        while (first <= secondNumber) {
            sum = sum + first;
            ++first;
            System.out.println(sum);
        }
    }
}