package application;

import java.util.Scanner;

public class Fibonacchi {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter interval: ");
        int interval = scanner.nextInt();
        int first = 1;
        int second = first + interval;
        while (second < 200) {
            int save = second;

            second = save + second;
            System.out.println(first);
        }
    }
}
